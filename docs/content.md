# Content

## Console file structure visualization

The command `>h5cross tree `  prints the structure of an HDF5 file in the console. 

![tree_output](tree.png)



## Statistical information of file

The command `>h5cross stats`  computes the mean, min, max, median and standard deviation of every data set for a given HDF5 file.  It is then output into the console (can be optionally deactivated). The result can be optionally saved in a YAML format.

![stats_tree_output](stats.png)



## Save file structure

The command `>h5cross dump`  outputs the structure of an HDF5 file in a YAML format (.yml). 



## View file structure

The command `>h5cross view`  allows an interactive view of the structure of an HDF5 file. It relies on the [nobvisual]( https://pypi.org/project/nobvisual/ ) package designed for the visualisation of nested objects.  A temporary .yml file is generated to allow the interactive use.  

![view](view.png)





## Compare file structures

The command `>h5cross diff` compares the structure of two HDF5 files. Similarily to  `>h5cross view` it relies on the [nobvisual]( https://pypi.org/project/nobvisual/ ) package.  It is optionally possible to add the statistical information of both files in this representation. 



![diff](diff.png)



## Compare field data 

The command `>h5cross scatter` allows the scatter plot comparison of selected data fields from two HDF5 files. If the data array lengths of each file differ an histogram representation will be given instead. 
A minimum requirement is the matplotlib package. By default it uses the seaborn package if available but this setting can be optionally deactivated. 



![seaborn](seaborn_same_T.png)

![plt](plt_same_T.png)

![hist](hist.png)





---

---

## Generating a random HDF5 file

A utility is available to generate random nested HDF5 files with random data (currently limited to two nested levels). It is intended to allow the user to quickly and easily test the above functionalities. The associated command is `>h5cross generate` . 