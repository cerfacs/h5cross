# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).


## [0.2.1] 2021 / 12 / 17

### Changed
* CLI options for handling output files

### Fixed
* stats on h5 files that contain non-numeric data


## [0.2.0] 2021 / 11 / 26

### Added
* hdfdict has been moved in


### Changed
* API has been thoroughly redesigned with little thought on backwards compatibility (most of the functionality has been kept and/or enhanced)
* better handling of plotting backends
* better handling of convert file formats


### Removed
* `generate` utils



## [0.1.3] 2021 / 10 / 20

### Changed
* Creation of setup.cfg to meet new coop standard


## [0.1.2] 2021 / 10 / 20

### Added
* add-xy flag for scatter plot: plots x = y line on top of the scatter
* added L2 Norm in scatter plot

### Changed
* remove imports from __init__


## [0.1.1] 2021 / 05 / 12

### Changed
* Allow single valued entries to be shown in stats regardless of its type if it can be computed 

### Fixed
* remove depreciated test check_environmental_modules for tools



## [0.1.0] 2021 / 03 / 17

### Added
* CLI `convert` Conversion to hdf5 of certain file formats.
* Conversion of nek5000 to hdf5 - first version
* Conversion of pvtu (ALYA output) to hdf5 - first version
* Conversion of vtu (ALYA output) to hdf5 - first version
* Assertion check of filetype for vtu and pvtu
* Conversion of vtk (OpenFOAM output) to hdf5 - first version

### Changed
* import of nobvisual performed globally with try except and global variable assigned
* import of matplotlib and seaborn performed globally with try except and global variable assigned
* import of scipy and PrettyTable performed globally with try except and global variable assigned
* import of pymech and vtk performed globally with try except and global variable assigned


## [0.0.1] 2021 / 02 / 17

### Added
* Pretty table output of statistics (default = True)

### Changed
* moved check_environmental_modules to tools
* upgrade scatter plots with density color maps
* update histogram and set nbins to 'auto''
* update docs and README.md



## [0.0.0] 2021 / 02 / 03

### Added
* CLI centered on `>h5cross`
* `tree` print hdf5 file structure to console
* `dump` save hdf5 file as YAML
* `view` interactive view of an hdf5 file
* `stats` compute basic statistics of a given hdf5 file fields
* `diff` interactive comparison of two hdf5 files
* `scatter` allows scatter plot comparison of hdf5 file fields
* `generate` creates an hdf5 file from randomly created nested dictionary
* CLI help messages for current functionalities
* try import modules checks in view_h5.py
* docs: added content brief introduction and illustration
* tests for visit_h5, generate_h5, tools and write_h5

### Changed
* README.md: add succint description of h5cross 
* add_numpy_statistics(): updated
* add matplotlib scatterplot
* rename file2yaml to write_h5
* add optional use of seaborn in scatter

### Fixed
* extend_dict(): corrected to avoid unwanted "dtype" and "value" in nested objects 

