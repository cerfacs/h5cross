
import numpy as np


# TODO: need to create more robust example

TEST_DICT = {
    'Extra': {
        'H2 mass fraction': np.array([0.90756983, 0.88504841, 0.38959945, 0.13252356, 0.55166653,
                                      0.43762069, 0.79879475, 0.08691671, 0.63710571, 0.76582934,
                                      0.08493008, 0.46838832, 0.5238339, 0.09711192, 0.93432919,
                                      0.73665159, 0.64406299, 0.59318919, 0.34395808, 0.6289766]),
        'Extra': {
            'O2 mass fraction': np.array([0.03239093, 0.93529074, 0.32835203, 0.29210036, 0.37117862,
                                          0.06437929, 0.99870701, 0.9427795, 0.98691639, 0.78114612,
                                          0.13389508, 0.73483166, 0.84075371, 0.67672449, 0.80263217,
                                          0.45433071, 0.51674349, 0.86227128, 0.87776688, 0.14653308])
        }
    },
    'pressure': np.array([166010.01894963, 251976.39803857, 210212.89259099, 293926.05215838,
                          221722.68953524, 178109.52485377, 130311.97285885, 396470.02240819,
                          238694.25466208, 390200.12727075, 142312.57707086, 132578.03745981,
                          322795.99283997, 232137.71316293, 294835.30866519, 124977.88001827])
}
