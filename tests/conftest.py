import os
from distutils import dir_util
from pathlib import Path

import pytest
from h5cross.same_nob import h5_same

# NOTE: h5_same from same_nob checks the actual values in the nobs.
#       h5same_files from h5_checksamefiles.py only checks the structure of the nobs.


@pytest.fixture(scope='module')
def datadir(tmpdir_factory, request):
    '''
    Fixture responsible for searching a folder with the same name as test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.
    '''
    file_path = request.module.__file__
    test_dir, _ = os.path.splitext(file_path)
    dir_name = os.path.basename(test_dir)

    datadir_ = tmpdir_factory.mktemp(dir_name)
    dir_util.copy_tree(test_dir, str(datadir_))

    return datadir_


@pytest.fixture(scope='session')
def meshes_dir():
    return Path(__file__).parent / "meshes"


# h5file comparison tool
@pytest.fixture(scope='module')
def h5same_files():
    return h5_same
