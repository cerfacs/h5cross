.. h5cross documentation master file, created by
   sphinx-quickstart on Mon Jan 20 13:57:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the h5cross documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme_copy
   # content
   api/h5cross


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
